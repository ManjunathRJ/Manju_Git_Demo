package com.vishusoft.corejava.InterfaceDemo;

public abstract class Account {
	protected double balance;
	
	public Account(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Account [balance=" + balance + "]";
	}
	
	
	

}
