package com.vishusoft.corejava.CollectionsDemo;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MapDemo {
	public static void main(String[] args) {
		Map<String,String> map = new TreeMap<>();
		map.put("s001", "Blue Polo Shirt");
		map.put("s002", "Black Polo Shirt");
		map.put("s003", "Pink Polo Shirt");
		map.put("h004", "Red Polo Shirt");
		map.put("s002", "Black t-Shirt");
		
		Set<String>  keys = map.keySet();
	
		System.out.println("----Part List");
		
		for(String key : keys ){
			System.out.println("Part # " + key +" " + map.get(key));
		}
	}

}
