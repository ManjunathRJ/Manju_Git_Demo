package com.vishusoft.corejava.CollectionsDemo.ComparatorDemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TestComparator {

	public static void main(String[] args) {

		List<Student> sList = new ArrayList<>();
		
		Comparator<Student> studCom = new StudentSortName();
		
		sList.add(new Student("Vishwa", 101, 9.9));
		sList.add(new Student("Anupama", 102, 8.9));
		sList.add(new Student("Zeera", 103, 5.4));
		sList.add(new Student("Birbal", 105, 2.4));
		sList.add(new Student("Manu", 104, 8.9));
		Collections.sort(sList, studCom);
		
		System.out.println("++ Sorted by Name");

		
		for(Student student : sList){
			System.out.println(student.getName());
		}
	}

}
