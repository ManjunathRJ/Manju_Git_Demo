package com.vishusoft.corejava.CollectionsDemo.ComarableDemo;

import java.util.Set;
import java.util.TreeSet;

public class TestComparable {

	public static void main(String[] args) {

		Set<ComparableStudent> set = new TreeSet<>();
		set.add(new ComparableStudent("Vishwanath", 101, 9.9));
		set.add(new ComparableStudent("Harish", 102, 6.5));
		set.add(new ComparableStudent("Chandru", 104, 8.2));
		
		for(ComparableStudent ss: set){
			System.out.println(ss);
		}
	}

}
