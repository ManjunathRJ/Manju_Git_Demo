
package com.vishusoft.corejava.CollectionsDemo;

import java.util.Set;
import java.util.TreeSet;

public class SetDemo {
	public static void main(String[] args) {
		Set<String> set = new TreeSet<>();
		set.add("one");
		set.add("two");
		set.add("three");
		set.add("one");
		
		for(String str : set){
			System.out.println(str);
		}
	}

}
