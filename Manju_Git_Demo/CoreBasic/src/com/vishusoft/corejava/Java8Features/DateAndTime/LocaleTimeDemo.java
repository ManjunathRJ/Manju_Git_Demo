package com.vishusoft.corejava.Java8Features.DateAndTime;

import java.time.LocalTime;

public class LocaleTimeDemo {

	public static void main(String[] args) {

		LocalTime  now , nowPlus, nowHrsMins, lunch,bedtime;
		
		now = LocalTime.now();
		System.out.println("Time Now : " +now);
		
		nowPlus = now.plusHours(1).plusMinutes(15);
		System.out.println("What is the time after 1 hour 15 mins from now :" +nowPlus );
		
		//nowHrsMins = now.truncatedTo(MINUTES);
	}

}
