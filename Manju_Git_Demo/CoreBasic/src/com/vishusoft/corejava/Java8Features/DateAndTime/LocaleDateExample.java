package com.vishusoft.corejava.Java8Features.DateAndTime;

import java.time.LocalDate;

public class LocaleDateExample {
	public static void main(String[] args) {
		LocalDate now, bDate , nowPlusMonth, nextTues;
		
		now = LocalDate.now();
		
		System.out.println("Now : "+now);
		
		bDate = LocalDate.of(1987, 8, 4);
		System.out.println("Vishu Birthday " + bDate);
		System.out.println("Is Vishu Birthday in the past? ..." + bDate.isBefore(bDate));
		System.out.println("is Vishu Birthday in LAEP year :..  " + bDate.isLeapYear());
		
		System.out.println("Vishu Birthday day of the week :  " + bDate.getDayOfWeek());
		
		
		nowPlusMonth = now.plusMonths(1);
		System.out.println("The day of the next month :" +nowPlusMonth);
		
		//nextTues= now.with(next(TUESADY));
		
		
	}

}
