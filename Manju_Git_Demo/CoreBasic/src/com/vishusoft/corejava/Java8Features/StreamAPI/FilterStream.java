package com.vishusoft.corejava.Java8Features.StreamAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FilterStream {

	public static void main(String[] args) {

		Product product = new Product();
		product.setId(121);
		product.setName("Mango");
		product.setPrice(2000);
		
		Product product1 = new Product();
		product.setId(122);
		product.setName("Banana");
		product.setPrice(251);
		
		Product product2 = new Product();
		product.setId(123);
		product.setName("Coconut");
		product.setPrice(250);
		
		Product product3 = new Product();
		product.setId(124);
		product.setName("Watch");
		product.setPrice(200);
		
		Product product4 = new Product();
		product.setId(125);
		product.setName("Shirt");
		product.setPrice(200);
		
		List<Product> list = new ArrayList<>();
		list.add(product);
		list.add(product1);
		list.add(product2);
		list.add(product3);
		list.add(product4);
		

		list.stream()
						.filter(p -> p.getPrice() == 200)
						.forEach(p -> System.out.println(p.getName()));
						
		
		//System.out.println(prodPrice);
	}

}
