package com.vishusoft.corejava.Java8Features.LambdaExp.NoParam;

public interface ISum {

	public int sum(int a, int b);
}
