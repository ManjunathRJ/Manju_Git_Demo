package com.vishusoft.corejava.Java8Features.LambdaExp.WithLambda;

import java.awt.Button;
import java.awt.Frame;

public class ButtonWithLambda {
	public static void main(String[] args) {
		
		Frame frame = new Frame("Action Listner after 8");
		Button button = new Button("Click Here");
		
		button.setBounds(50, 80, 100, 50);
		
		button.addActionListener(e-> System.out.println("Hello World"));
		
		frame.add(button);
		frame.setSize(200,200);
		frame.setLayout(null);
		frame.setVisible(true);
	}

}
