package com.vishusoft.corejava.Java8Features.LambdaExp.NoParam;

public class ClientSum {
	public static void main(String[] args) {
		
		ISum isum = (int a, int b) ->  a+b;
		
	 System.out.println(isum.sum(10, 20));
	}

}
