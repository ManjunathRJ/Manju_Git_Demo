package com.vishusoft.corejava.Java8Features.LambdaExp.NoParam;

public class TestClient {
	public static void main(String[] args) {
		
		IFunctional ifun = () -> {
			return "Hello";
		};
		System.out.print(ifun.sayHello());
	}

}
