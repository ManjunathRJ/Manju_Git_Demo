package com.vishusoft.corejava.Java8Features.LambdaExp.WithoutLambda;

import java.awt.Button;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListner {
	public static void main(String[] args) {
		Frame frame = new Frame("Action Listner before 8");
		Button button = new Button("Click Here");
		
		button.setBounds(50, 100, 80, 50);
		
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				System.out.println("Hello World!..");
			}
		});
		
		frame.add(button);
		frame.setSize(200,200);
		frame.setLayout(null);
		frame.setVisible(true);
	}

}
